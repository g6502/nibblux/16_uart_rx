library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity UART_RX is
	generic	(	clk_freq	: Integer	:= 50e6;
					Baudrate	: Integer	:= 9600;
					Divisor	: Integer	:= 16
	);
	port	(	clk	: in	std_logic;
				Rx		: in	std_logic;
				Busy	: out	std_logic;
				Byte	: out	std_logic_vector(7 downto 0)
	);
end UART_RX;

architecture behave of UART_RX is

	type	maquina_rx is (espera,recibe);
	signal	arranque	: std_logic := '0';
	signal	buff		: std_logic_vector(7 downto 0);
	
begin
	process(clk)
		variable cont	: integer range 0 to clk_freq/baudrate/Divisor-1 := 0;
	begin
		if rising_edge(clk) then 
			if cont < clk_freq/baudrate/divisor-1 then
				cont := cont + 1;
				arranque <= '0';
			else
				cont := 0;
				arranque <= '1';
			end if;
		end if;
	end process;
	
	process(clk, arranque)
		variable estado	: maquina_rx := espera;
		variable cont_d	: integer range 0 to divisor	:= 0;
		variable cont_rx	: integer range 0 to 8	:= 0;
	begin
		if rising_edge(clk) and arranque = '1' then
			case estado is
				when espera =>
					busy <= '0';
					if rx = '0' then
						if cont_d < divisor/2 then
							cont_d := cont_d + 1;
							estado := espera;
						else
							cont_d	:= 0;
							cont_rx	:= 0;
							busy	<= '1';
							estado	:= recibe;
						end if;
					else
						cont_d	:= 0;
						estado	:= espera;
					end if;
					
				when recibe =>
					if cont_d < divisor - 1 then
						cont_d := cont_d + 1;
						estado := recibe;
					elsif cont_rx < 8 then
						cont_d := 0;
						cont_rx := cont_rx + 1;
						buff <= rx & buff(7 downto 1);
						estado := recibe;
					else
						byte <= buff;
						busy <= '0';
						estado := espera;
					end if;	
			end case;
		end if;
	end process;
end behave;